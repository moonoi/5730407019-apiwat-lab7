

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


@WebServlet(name = "CallFac", urlPatterns = {"/CallFac"})
public class CallFac extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
           
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CallFac</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>These are faculties at KKU</h1>");
            out.println("<ul>");
            JSONParser parser = new JSONParser();
            
            
            
            try {
                URL url = new URL("http://www.kku.ac.th/ikku/api/academics/services/getFaculty.php");
                URLConnection urlConnect = url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnect.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String inputLine;
                String StrJ = "";
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine);
                }
                in.close();
                StrJ = sb.toString();

                
                JSONObject obj = (JSONObject) parser.parse(StrJ);
                JSONArray results = (JSONArray) obj.get("results");
                Iterator itr = results.iterator();
                
                while (itr.hasNext()) {
                    JSONObject innerObj = (JSONObject) itr.next();
                    String title = (String) innerObj.get("name_en");
                    out.println("<li>" + title + "</li>");
                }
            } catch (Exception e) {
                System.err.println("There's an error.");
            }

            out.println("</ul>");
            out.println("</body>");
            out.println("</html>");
        }
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(CallFac.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
