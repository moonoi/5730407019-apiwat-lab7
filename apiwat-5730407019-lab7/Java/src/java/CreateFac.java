
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


@WebServlet(urlPatterns = {"/CreateFac"})
public class CreateFac extends HttpServlet {

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
           
            JSONParser parser = new JSONParser();
            try {
                URL url = new URL("http://www.kku.ac.th/ikku/api/academics/services/getFaculty.php"); 
                URLConnection UrlConnect = url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(UrlConnect.getInputStream()));
                StringBuilder s = new StringBuilder();
                String input;
                String StrJ = "";
                while ((input = in.readLine()) != null) {
                    s.append(input);
                }
                in.close();
                StrJ = s.toString();
                
                JSONObject JsonObj = (JSONObject) parser.parse(StrJ);
                JSONArray results = (JSONArray) JsonObj.get("results");
                JSONArray faculties = new JSONArray();
                
                for (int i = 0; i < 2; i++){
                    Random random = new Random();
                    int selInt = random.nextInt((26 - 0) + 1) + 0;
                    JSONObject selObj = (JSONObject)results.get(selInt);
                    JSONObject faculty = new JSONObject();
                    
                    
                    faculty.put("name", selObj.get("name_en"));
                    faculty.put("id", selObj.get("id"));
                    faculties.add(faculty);
                }
                JSONObject DocJson = new JSONObject();
                DocJson.put("faculties", faculties);
                out.println(DocJson);
                
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(CreateFac.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


}
