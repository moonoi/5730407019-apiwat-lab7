<?php
$url = "https://www.kku.ac.th/ikku/api/academics/services/getFaculty.php";
$json = file_get_contents($url);
$obj = json_decode($json);

$root = $obj->results;
$count = sizeof($root);
$i = 0;
?>

<html>
    <body>
        <h1>These are faculties at KKU</h1>
        <ul>
            <?php
            while ($i < $count) {
                ?>
                <li>
                    <?php
                    echo $root[$i]->name_en;
                    $i++;
                }
                ?>
            </li>

        </ul>
    </body>
</html>