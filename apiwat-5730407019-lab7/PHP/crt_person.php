<?php

$CreateAr = array(
    "name" => "apiwat",
    "addr" => array("street" => "Computer Engineering Dept., KKU",
        "zipcode" => "40002"),
    "phones" => array("011-111-1111", "11-111-1111")
);

$allc = json_encode($CreateAr, JSON_PRETTY_PRINT);
header('Content-Type: application/json');

echo $allc;
?>